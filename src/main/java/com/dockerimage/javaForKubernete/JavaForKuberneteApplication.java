package com.dockerimage.javaForKubernete;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@SpringBootApplication
@PropertySource("classpath:Property1.properties")
public class JavaForKuberneteApplication {

		public static void main(String[] args) {

		SpringApplication.run(JavaForKuberneteApplication.class, args);
	}
}
