package com.dockerimage.javaForKubernete;

import com.dockerimage.javaForKubernete.exceptions.InternalServerError;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;


/**
 * The challenge is as below:
 *
 * The desired behavior is to have Kubemetes restart the pod when an endpoint returns an HTTP 500 on the /healthz endpoint.
 * The service, probe-pod, should never send traffic to the pod while it is failing. Please complete the following:
 * • The application has an endpoint, /started, that will indicate if it can accept traffic by returning an HTTP 200. If the endpoint returns an HTTP 500, the application has not yet finished initialization.
 * • The application has another endpoint /healthz that will indicate if the application is still working as expected by returning an HTTP 200. If the endpoint returns an HTTP 500 the application is no longer responsive.
 * • Configure the probe-pod pod provided to use these endpoints • The probes should use port 808
 *
 * */
@RestController
public class Endpoint1 {

    @Value( "${started.sleep}" )
    private int seconds;

    /**
     * It randomly returns HTTP code 500 or 200.
     * */
    @GetMapping("/healthz")
    public ResponseEntity<Object> healthy()
    {
        if(new Random().nextInt()%2==0) {
            return new ResponseEntity<Object>("internal server error in healthy endpoint", HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<Object>("healthy", HttpStatus.OK);
    }

    /**
     * It sleeps several seconds, which is defined in a property file.
     * It randomly returns HTTP code 500 or 200.
     * */
    @GetMapping("/started")
    public ResponseEntity<Object> started()
    {
        try {
            while(seconds-->=0){
                Thread.sleep(1000);
            }
            if(new Random().nextInt()%2==0) {
                return new ResponseEntity<Object>("internal server error in started endpoint", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new ResponseEntity<Object>("/started endpoint succeed", HttpStatus.OK);
    }

}
