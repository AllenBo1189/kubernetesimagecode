FROM adoptopenjdk/openjdk13:jdk-13.0.2_8-ubuntu-slim

#this sets the file name of application property
ENV applicationProperty=dev
WORKDIR /app
COPY build/libs/javaForKubernete-0.0.1.jar /app/app.jar
COPY entry.sh entry.sh
RUN chmod 700 entry.sh && chown root:root /app/app.jar
ENTRYPOINT [ "/app/entry.sh" ]