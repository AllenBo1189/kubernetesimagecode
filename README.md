#Description
The intention of the project is to create a mock environment of kubernetes to help pass CKAD exam.

#Setup
##Java version
13

##buildRunLocally.sh
This file builds and runs the application locally, not in docker.
<br/>The env variable, applicationProperty, specifies the alternative file name for application.properties. So it can dynamically
replace the property file.

#Endpoints
/healthz 
    <br/>Its return code is defined in Property1.properties

